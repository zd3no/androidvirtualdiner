package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Profile extends ActionBarActivity {
	private EditText txtName;
	private EditText txtSurname;
	private EditText txtPasswordOriginal;
	private EditText txtPassword;
	private EditText txtPasswordConfirm;
	private EditText txtAddress;
	private EditText txtPhone;
	private EditText txtEmail;
	private EditText txtEmailConfirm;
	private List<NameValuePair> nameValuePair;
	private TextView tvName;
	private TextView tvSurname;
	private TextView tvPhone;
	private TextView tvAddress;
	private TextView tvEmail;
	private TextView tvBalance;
	private LinearLayout linlaHeaderProgress;
	private Button btnShowEditArea;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		linlaHeaderProgress = (LinearLayout) findViewById(R.id.ll_profile);
		btnShowEditArea = (Button)findViewById(R.id.btn_profile_edit);
		populateCurrentDetailsInTextViews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void startEditingUserDetails(View view){
		btnShowEditArea.setVisibility(View.GONE);
		linlaHeaderProgress.setVisibility(View.VISIBLE);

		populateExistingDataToEditTexts();
	}
	
	//create references to buttons
	public void createReferences(){
		txtName = (EditText)findViewById(R.id.txtProfileName);
		txtSurname = (EditText)findViewById(R.id.txtProfileSurname);
		txtPassword = (EditText)findViewById(R.id.txtProfilePassword);
		txtPasswordConfirm = (EditText)findViewById(R.id.txtProfilePasswordConfirm);
		txtAddress = (EditText)findViewById(R.id.txtProfileAddress);
		txtPhone = (EditText)findViewById(R.id.txtProfilePhone);
		txtEmail = (EditText)findViewById(R.id.txtProfileEmail);
		txtEmailConfirm = (EditText)findViewById(R.id.txtProfileEmailConfirm);
		txtPasswordOriginal = (EditText)findViewById(R.id.txtProfilePasswordOriginal);
		tvName = (TextView)findViewById(R.id.tv_profile_name_text);
		tvSurname = (TextView)findViewById(R.id.tv_profile_surname_text);
		tvPhone = (TextView)findViewById(R.id.tv_profile_phone_text);
		tvAddress = (TextView)findViewById(R.id.tv_profile_address_text);
		tvEmail = (TextView)findViewById(R.id.tv_profile_email_text);
		tvBalance = (TextView)findViewById(R.id.tv_profile_balance_text);
	}
	
	public void populateExistingDataToEditTexts(){
		//populate data to edit texts
		txtName.setText(Service.userName);
		txtSurname.setText(Service.userSurname);
		txtAddress.setText(Service.userAddress);
		txtPhone.setText(Service.userPhone);
		txtEmail.setText(Service.userEmail);
		txtEmailConfirm.setText(Service.userEmail);
	}
	
	public void populateCurrentDetailsInTextViews(){
		createReferences();
		tvName.setText(Service.userName);
		tvSurname.setText(Service.userSurname);
		tvPhone.setText(Service.userPhone);
		tvAddress.setText(Service.userAddress);
		tvEmail.setText(Service.userEmail);
		tvBalance.setText(Double.toString(Service.userBalance));
	}
	
	public void editDetails(View view){
		if(fieldsAreValid()){
			nameValuePair = generateNameValuePairs();
			if(isOnline()){
				//create and call POST request
				String restMethodURL = "updateUser";
				new HttpAsyncTask().execute(restMethodURL);
			}else{
				Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}
	
	public boolean fieldsAreValid(){
		removeErrors();
		if(txtName.getText().toString().equals("")){
			txtName.setError("Name cannot be null");
			return false;
		}else if(!txtName.getText().toString().matches(Service.NAME_PATTERN)){
			txtName.setError("Invalid name");
			return false;
		}else if(txtSurname.getText().toString().equals("")){
			txtSurname.setError("Surname cannot be null");
			return false;
		}else if(!txtSurname.getText().toString().matches(Service.NAME_PATTERN)){
			txtSurname.setError("Invalid surname");
			return false;
		}else if(txtPhone.getText().toString().equals("")){
			txtPhone.setError("Phone cannot be null");
			return false;
		}else if(!txtPhone.getText().toString().matches(Service.PHONE_PATTERN)){
			txtPhone.setError("Invalid phone number");
			return false;
		}else if(txtAddress.getText().toString().equals("")){
			txtAddress.setError("Address cannot be null");
			return false;
		}else if(txtEmail.getText().toString().equals("")){
			txtEmail.setError("Email cannot be null");
			return false;
		}else if(!txtEmail.getText().toString().matches(Service.EMAIL_PATTERN)){
			txtEmail.setError("Invalid Email!");
			return false;
		}else if(txtPasswordOriginal.getText().length() == 0){
			txtPasswordOriginal.setError("You must confirm your actions with current password");
			return false;
		}else if(txtPasswordOriginal.getText().length() < 6){
			txtPasswordOriginal.setError("This password is invalid");
			return false;
		}else if(!txtEmail.getText().toString().equals(txtEmailConfirm.getText().toString())){
			txtEmailConfirm.setError("Emails do not match!");
			txtEmailConfirm.setText("");
			return false;
		}else if(!txtPassword.getText().toString().equals(txtPasswordConfirm.getText().toString())){
			txtPasswordConfirm.setError("New passwords do no match!");
			txtPasswordConfirm.setText("");
			return false;
		}else{
			return true;
		}
	}
	
	public void removeErrors(){
		txtName.setError(null);
		txtSurname.setError(null);
		txtPhone.setError(null);
		txtAddress.setError(null);
		txtEmail.setError(null);
		txtPassword.setError(null);
	}
	
	//create the nameValuePairs needed for POST request
	public List<NameValuePair> generateNameValuePairs(){
    	List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
		nameValuePair.add(new BasicNameValuePair("name", txtName.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("surname", txtSurname.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("phone", txtPhone.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("address", txtAddress.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("currentEmail", Service.userEmail));
		nameValuePair.add(new BasicNameValuePair("currentPassword", txtPasswordOriginal.getText().toString()));
		if(txtEmail.getText().length() > 0){
			nameValuePair.add(new BasicNameValuePair("newEmail", txtEmail.getText().toString()));
		}else{
			nameValuePair.add(new BasicNameValuePair("newEmail", Service.userEmail));
		}
		if(txtPassword.getText().length() > 0){
			nameValuePair.add(new BasicNameValuePair("newPassword", txtPassword.getText().toString()));
		}else{
			nameValuePair.add(new BasicNameValuePair("newPassword", txtPasswordOriginal.getText().toString()));
		}
		return nameValuePair;
	}
	
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return Service.POST(urls[0], nameValuePair);
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	Log.d("Response is: ",result);
        	//expected result: [[email, name, surname, phone, password(encoded),address, balance(double)]] 
        	try {
				JSONArray array = new JSONArray(result);
				Service.userEmail = array.getJSONArray(0).getString(0);
				Service.userName = array.getJSONArray(0).getString(1);
				Service.userSurname = array.getJSONArray(0).getString(2);
				Service.userPhone = array.getJSONArray(0).getString(3);
				Service.userAddress = array.getJSONArray(0).getString(5);
				Service.userBalance = array.getJSONArray(0).getDouble(6);
				
				btnShowEditArea.setVisibility(View.VISIBLE);
				linlaHeaderProgress.setVisibility(View.GONE);
				populateCurrentDetailsInTextViews();
				Toast.makeText(getApplicationContext(), "Your details have been updated", Toast.LENGTH_SHORT).show();
			} catch (JSONException e) {
				e.printStackTrace();
				if(e.getMessage().contains("Cannot delete or update")){
					txtEmail.setError("Email belongs to another user! Try again");
				}
				Toast.makeText(getApplicationContext(), "Invalid authentication! Try again", Toast.LENGTH_SHORT).show();
			}	
       }
    }//end HttpAsyncTask
	
}
