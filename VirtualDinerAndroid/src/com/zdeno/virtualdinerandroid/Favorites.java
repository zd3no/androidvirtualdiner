package com.zdeno.virtualdinerandroid;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.zdeno.virtualdinerandroid.CurrentOrder.MyListAdapter;
import com.zdeno.virtualdinerandroid.support.CompleteItem;

public class Favorites extends ActionBarActivity {

	ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();
	private ListView lv;
	private ListAdapter adapter;
	private View.OnTouchListener touchListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorites);
		lv = (ListView) findViewById(R.id.favorite_listview);

		fetchFavoritesFromServer();

	}

	private void fetchFavoritesFromServer() {
		try {
			String restUrl="fetchFavorites?email="+URLEncoder.encode(Service.userEmail,"UTF-8");
			new FetchFavoritesAsyncTask().execute(restUrl);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private class FetchFavoritesAsyncTask extends AsyncTask<String, Void, String> {
    		
        @Override
        protected String doInBackground(String... urls) {
            return Service.GET(urls[0]);
        }
        
        @Override
        protected void onPostExecute(String result) {
            Log.d("Response is: ",result);
            if(!result.equals("ERROR")){
            	//if nothing is returned, user does not have any favorites
            	if(result.length() == 0){
            		Toast.makeText(getApplicationContext(), "You do not have any favourites yet", Toast.LENGTH_SHORT).show();
            		return;
            	}
            	//populate Service.favorites from the result
            	try {
            	JSONArray array = new JSONArray(result);
            	Service.favorites = new ArrayList<CompleteItem>();
            	for(int i=0; i<array.length(); i++){
					JSONArray x = array.getJSONArray(i);
					for(CompleteItem item:Service.items){
						if(item.getId() == (int)x.getInt(1)){
							Service.favorites.add(item);
						}
					}
            	}
            	
            	//display favorite items in the listview
            	displayItems();
            	return;
            	} catch (JSONException e) {
            		Toast.makeText(getApplicationContext(), "Couldn't load favorites", Toast.LENGTH_SHORT).show();
            		return;
            	}
            	
            }else{
            	Toast.makeText(getApplicationContext(), "Couldn't load favorites", Toast.LENGTH_SHORT).show();
				return;
            }
       }
    }//end FetchFavoritesAsyncTask

	private void displayItems() {
		// put products from the favorites array to an array of hashmaps to be displayed in the list view
		for (CompleteItem item : Service.favorites) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", item.getName());
			map.put("description", item.getDescriptionShort());
			itemList.add(map);
		}
		updateListView();
	
	//touchListener
	touchListener = new View.OnTouchListener() {
		
		private float downX;
		private int touchSlop = -1;
		private boolean itemPressed = false;
		private boolean swiping = false;
		View thisView;
		
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		public boolean onTouch(View v, MotionEvent event) {
			thisView = v;
			//get the touch slop of the current device (the amount of pixels needed to move finger on screen in order to be recorded as scroll action 
			if(touchSlop < 0)
				touchSlop = ViewConfiguration.get(v.getContext()).getScaledTouchSlop();
			
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				//check if the user pressed more than one item. If so then return false and do not handle it
				if(itemPressed)
					return false;
				itemPressed = true;
				downX = event.getX();
				break; 
			}
			case MotionEvent.ACTION_CANCEL:{
				v.setAlpha(1); //sets the opacity of the view to 100%
				v.setTranslationX(0); //sets the horizontal location (left) of this view to 0 points in relation to its parent
				itemPressed = false;
				break;
			}
			case MotionEvent.ACTION_MOVE: {
				float x = event.getX() + v.getTranslationX(); //get the touch position in relation to it's current location in a parent
				float deltaX = x - downX; //difference between current location and initial touch
				float deltaAbsX = Math.abs(deltaX); // get the absolute number, ignore (-) 
				
				//check whether we are swiping or not
				if(swiping == false){
					if(deltaAbsX > touchSlop){
						swiping = true;
						//tell the parent to stop intercepting other swipe events so that this view has full attention
						v.getParent().requestDisallowInterceptTouchEvent(true);
					}
				}
				if(swiping == true){
					v.setTranslationX(x-downX);
					v.setAlpha(1 - deltaAbsX / v.getWidth());
				}
				break;
			}
			case MotionEvent.ACTION_UP: {
				if(swiping == true){
					int position = 0;
					float x = event.getX() + v.getTranslationX();
					float deltaX = x - downX;
					float deltaAbsX = Math.abs(deltaX);
					if(deltaAbsX > v.getWidth() / 3){
						LinearLayout ll = (LinearLayout) v;
						TextView txtName = (TextView)ll.getChildAt(0);
						
						for(int i=0; i< adapter.getCount(); i++){
							HashMap<String, String> map = (HashMap<String, String>) adapter.getItem(i);
							if(map.get("name").equalsIgnoreCase(txtName.getText().toString())){
								position = i;
								new DeleteFavoriteFromServer().execute(txtName.getText().toString());
								Log.d("itemList","Removed item from position: "+i+". The item was: "+Service.favorites.get(position).getName());
								itemList.remove(position);
								Service.favorites.remove(position);
								MyListAdapter ad = (MyListAdapter)lv.getAdapter();
								ad.notifyDataSetChanged();
								v.setTranslationX(0);
								v.setAlpha(1);
								break;
							}
						}
					}else{
						v.setTranslationX(0);
						v.setAlpha(1);
					}
				}else{
					LinearLayout ll = (LinearLayout) v;
					TextView txtName = (TextView)ll.getChildAt(0);
					Service.selectedFavoriteItem = txtName.getText().toString();
					
					//redirect
					Intent intent = new Intent(Favorites.this, FavoritePager.class);
					startActivity(intent);
				}
				swiping = false;
				itemPressed = false;
				break;
			}
			default:{
				return false;
			}
			}//end switch
			return true;
		}//end onTouch
	};
}
	
	//inflate the listView with items
	private void updateListView() {
		adapter = new MyListAdapter(Favorites.this, R.layout.list_item_item, itemList, touchListener);
		lv.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class MyListAdapter extends ArrayAdapter<HashMap<String, String>> {

		public MyListAdapter(Context context, int resource,
				List<HashMap<String, String>> objects, View.OnTouchListener touchListener) {
			super(context, resource, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				Log.d("VIEW", "v is null");
				LayoutInflater vi;
				vi = LayoutInflater.from(getContext());
				v = vi.inflate(R.layout.list_item_item, null);
				v.setOnTouchListener(touchListener);
			}

			HashMap<String, String> map = getItem(position);

			if (map != null) {

				TextView tv_name = (TextView) v
						.findViewById(R.id.list_item_name);
				TextView tv_description = (TextView) v
						.findViewById(R.id.list_item_description);


				if (tv_name != null) {
					tv_name.setText(map.get("name"));
				}
				if (tv_description != null) {
					tv_description.setText(map.get("description"));
				}
			}

			return v;
		}
	}
	
	private class DeleteFavoriteFromServer extends AsyncTask<String,String,String>{

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
			nameValuePair.add(new BasicNameValuePair("email", Service.userEmail));
			nameValuePair.add(new BasicNameValuePair("itemName", params[0]));
			return Service.POST("deleteFavorite",nameValuePair);
		}
		
		@Override
		protected void onPostExecute(String result) {
			
			super.onPostExecute(result);
		}
	}

}
