package com.zdeno.virtualdinerandroid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void toLogin(View view){
		Intent intent = new Intent(this, UserAuthentication.class);
		startActivity(intent);
	}
	
	public void browseMenu(View view){
		//check Internet access
		if(isOnline()){
			//check if the server is up and running
			new RedirectToMenu().execute("isServerUp");
		}else{
			Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	//http://stackoverflow.com/q/12752598
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}
	
	private class RedirectToMenu extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			return Service.GET(params[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			Log.d("RESULT", result);
			if(result.equalsIgnoreCase("NO CONNECTION"))
            	Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_SHORT).show();
            else{
				Intent intent = new Intent(MainActivity.this, OrderMenu.class);
				intent.putExtra(Service.USER_NAME, "");
				startActivity(intent);
            }
		}
	}
	
}
