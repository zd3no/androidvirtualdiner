package com.zdeno.virtualdinerandroid.support;

public class CompleteItem {
	private int id;
	private String size, name, allergy, category, descriptionLong, descriptionShort, grouping, imageName, ingredients;
	private double price, carbs, fat, fibre, kcalories, protein, salt, saturates, sugar;
	
	public CompleteItem(){
		
	}
	
	public CompleteItem(int id, double price, String size, String name, String allergy, String category, String descriptionLong, String descriptionShort, String grouping, 
			String imageName, String ingredients, double carbs, double fat, double fibre, double kcalories, double protein, double salt, double saturates, double sugar){
		this.id=id;
		this.price = price;
		this.size=size;
		this.name=name;
		this.allergy=allergy;
		this.category=category;
		this.descriptionLong = descriptionLong;
		this.descriptionShort=descriptionShort;
		this.grouping=grouping;
		this.imageName = imageName;
		this.ingredients=ingredients;
		this.carbs=carbs;
		this.fat=fat;
		this.fibre=fibre;
		this.kcalories=kcalories;
		this.protein=protein;
		this.salt=salt;
		this.saturates=saturates;
		this.sugar=sugar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllergy() {
		return allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescriptionLong() {
		return descriptionLong;
	}

	public void setDescriptionLong(String descriptionLong) {
		this.descriptionLong = descriptionLong;
	}

	public String getDescriptionShort() {
		return descriptionShort;
	}

	public void setDescriptionShort(String descriptionShort) {
		this.descriptionShort = descriptionShort;
	}

	public String getGrouping() {
		return grouping;
	}

	public void setGrouping(String grouping) {
		this.grouping = grouping;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getCarbs() {
		return carbs;
	}

	public void setCarbs(double carbs) {
		this.carbs = carbs;
	}

	public double getFat() {
		return fat;
	}

	public void setFat(double fat) {
		this.fat = fat;
	}

	public double getFibre() {
		return fibre;
	}

	public void setFibre(double fibre) {
		this.fibre = fibre;
	}

	public double getKcalories() {
		return kcalories;
	}

	public void setKcalories(double kcalories) {
		this.kcalories = kcalories;
	}

	public double getProtein() {
		return protein;
	}

	public void setProtein(double protein) {
		this.protein = protein;
	}

	public double getSalt() {
		return salt;
	}

	public void setSalt(double salt) {
		this.salt = salt;
	}

	public double getSaturates() {
		return saturates;
	}

	public void setSaturates(double saturates) {
		this.saturates = saturates;
	}

	public double getSugar() {
		return sugar;
	}

	public void setSugar(double sugar) {
		this.sugar = sugar;
	}
	
	
}
