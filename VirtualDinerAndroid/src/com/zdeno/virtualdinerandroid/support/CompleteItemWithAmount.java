package com.zdeno.virtualdinerandroid.support;

public class CompleteItemWithAmount extends CompleteItem{
	private int amount;
	
	public CompleteItemWithAmount(CompleteItem c, int amount){
		super(c.getId(), c.getPrice(), c.getSize(), c.getName(), c.getAllergy(), c.getCategory(), c.getDescriptionLong(), c.getDescriptionShort(), c.getGrouping(),
				c.getImageName(), c.getIngredients(), c.getCarbs(), c.getFat(), c.getFibre(), c.getKcalories(), c.getProtein(), c.getSalt(), c.getSaturates(), c.getSugar());
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
