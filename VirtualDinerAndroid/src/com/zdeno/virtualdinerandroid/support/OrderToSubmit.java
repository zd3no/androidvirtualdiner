package com.zdeno.virtualdinerandroid.support;

import org.json.JSONException;
import org.json.JSONObject;


public class OrderToSubmit {

	private String name, size, tableId; 
	private int amount, id;
	private float price, total;
	
	public JSONObject getJsonObjectEqvivalent(){
		JSONObject object = new JSONObject();
		try {
			object.put("name", name);
			object.put("size", size);
			object.put("amount", amount);
			object.put("id", id);
			object.put("price", price);
			object.put("total", total);
			object.put("tableId", tableId);
			return object;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public OrderToSubmit(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	
}
