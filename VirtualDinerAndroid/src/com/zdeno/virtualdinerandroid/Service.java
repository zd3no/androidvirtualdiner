package com.zdeno.virtualdinerandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import com.zdeno.virtualdinerandroid.support.CompleteItem;
import com.zdeno.virtualdinerandroid.support.CompleteItemWithAmount;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class Service {
	public static final String USER_NAME = "com.zdeno.virtualDiner.USER_NAME";
	public static final String USER_EMAIL = "com.zdeno.virtualDiner.USER_EMAIL";
	public static final String USER_PASSWORD = "com.zdeno.virtualDiner.USER_PASSWORD";
	public static String userName = "";
	public static String userEmail = "";
	public static String userSurname = "";
	public static String userPhone = "";
	public static String userAddress = "";
	public static double userBalance = 0.00;
	private static String local = "192.168.0.2";
	private static String publicAddress = "89.101.4.4";
	public static final String DOMAIN_URL = "http://"+publicAddress+":8080/Diner/rest/";
	public static final String DOMAIN_URL_IMAGES = "http://"+publicAddress+":8080/Diner/images/large/";
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String NAME_PATTERN = "[a-zA-Z\\s']*";
	public static final String PHONE_PATTERN = "[0-9 \\-\\+()]{9,15}";
	public static ArrayList<String> categories = new ArrayList<String>();
	static ArrayList<CompleteItem> items = new ArrayList<CompleteItem>();
	static ArrayList<CompleteItem> atomicItems = new ArrayList<CompleteItem>();
	public static String seletedCategory = "";
	public static String selectedItem = "";
	public static String selectedFavoriteItem = "";
	private static Context pagerContext;
	static ArrayList<CompleteItemWithAmount> currentOrder = new ArrayList<CompleteItemWithAmount>();
	static ArrayList<CompleteItem> favorites = new ArrayList<CompleteItem>();
	
	public Service() {
		
	}
	
	public static String GET(String methodAndParameters){
		InputStream inputStream = null;
        String result = "";
        try {
        	 
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(DOMAIN_URL+methodAndParameters));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            
            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "NO CONNECTION";
 
            return result;
        } catch (Exception e) {
            Log.d("InputStream", e.toString());
            return "NO CONNECTION";
        }
	}
	
	public static String POST(String method, List<NameValuePair> nameValuePair){
		InputStream inputStream = null;
        String result = "";
        try {
        	 
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
 
            // create HttpPost
            HttpPost httpPost = new HttpPost(DOMAIN_URL+method);
            
            //Encoding POST data
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            
            // make POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
 
            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "NO CONNECTION";
 
            return result;
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
            return "NO CONNECTION";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            return "NO CONNECTION";
        }
	}
	
	// convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
    
    //http://stackoverflow.com/a/6407554
    public static Drawable loadImageFromWebOperations(String url) {
        try {
        	URL urlToDownload = new URL(url);
        	URLConnection connection;
            connection = urlToDownload.openConnection();
            connection.connect();
            Object response = connection.getContent();
            InputStream is = (InputStream) response;
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

	public static void clearSession() {
		userAddress = "";
		userBalance = 0.00;
		userEmail = "";
		userName = "";
		userSurname = "";
		userPhone = "";
	}
	

	public static Context getPagerContext() {
		return pagerContext;
	}

	public static void setPagerContext(Context pagerContext) {
		Service.pagerContext = pagerContext;
	}

	public static String POST(String method, JSONArray array) {
		InputStream inputStream = null;
        String result = "";
        try {
        	 
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
 
            // create HttpPost
            HttpPost httpPost = new HttpPost(DOMAIN_URL+method);
            
            //Encoding POST data
            String stringifiedArray = array.toString();
            Log.d("STRINGIFIED", stringifiedArray);
            StringEntity entity = new StringEntity(stringifiedArray);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setEntity(entity);
            
            // make POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
 
            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
 
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
 
        return result;
	}
	
}
