package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends ActionBarActivity {
	
	private EditText txtName;
	private EditText txtSurname;
	private EditText txtPassword;
	private EditText txtPasswordConfirm;
	private EditText txtAddress;
	private EditText txtPhone;
	private EditText txtEmail;
	private EditText txtEmailConfirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
public void register(View view){
		
		txtName = (EditText)findViewById(R.id.txtName);
		txtSurname = (EditText)findViewById(R.id.txtSurname);
		txtPassword = (EditText)findViewById(R.id.txtPassword);
		txtPasswordConfirm = (EditText)findViewById(R.id.txtPasswordConfirm);
		txtAddress = (EditText)findViewById(R.id.txtAddress);
		txtPhone = (EditText)findViewById(R.id.txtPhone);
		txtEmail = (EditText)findViewById(R.id.txtEmail);
		txtEmailConfirm = (EditText)findViewById(R.id.txtEmailConfirm);
		
		if(fieldsAreValid()){
			if(isOnline()){
				//create and call POST request
				String restMethodURL = "registerUser";
				new HttpAsyncTask().execute(restMethodURL);
			}else{
				Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}

	public boolean fieldsAreValid(){
		removeErrors();
		if(txtName.getText().toString().equals("")){
			txtName.setError("Name cannot be null");
			return false;
		}else if(!txtName.getText().toString().matches(Service.NAME_PATTERN)){
			txtName.setError("Invalid name");
			return false;
		}else if(txtSurname.getText().toString().equals("")){
			txtSurname.setError("Surname cannot be null");
			return false;
		}else if(!txtSurname.getText().toString().matches(Service.NAME_PATTERN)){
			txtSurname.setError("Invalid surname");
			return false;
		}else if(txtPhone.getText().toString().equals("")){
			txtPhone.setError("Phone cannot be null");
			return false;
		}else if(!txtPhone.getText().toString().matches(Service.PHONE_PATTERN)){
			txtPhone.setError("Invalid phone number");
			return false;
		}else if(txtAddress.getText().toString().equals("")){
			txtAddress.setError("Address cannot be null");
			return false;
		}else if(txtEmail.getText().toString().equals("")){
			txtEmail.setError("Email cannot be null");
			return false;
		}else if(!txtEmail.getText().toString().matches(Service.EMAIL_PATTERN)){
			txtEmail.setError("Invalid Email!");
			return false;
		}else if(txtPassword.getText().toString().equals("")){
			txtPassword.setError("Password cannot be null");
			return false;
		}else if(txtPassword.getText().toString().length() < 6){
			txtPassword.setError("Password size must be at least 6");
			return false;
		}else if(!txtEmail.getText().toString().equals(txtEmailConfirm.getText().toString())){
			txtEmailConfirm.setError("Emails do not match!");
			txtEmailConfirm.setText("");
			return false;
		}else if(!txtPassword.getText().toString().equals(txtPasswordConfirm.getText().toString())){
			txtPasswordConfirm.setError("Passwords do no match!");
			txtPasswordConfirm.setText("");
			return false;
		}else{
			return true;
		}
	}
	
	public void removeErrors(){
		txtName.setError(null);
		txtSurname.setError(null);
		txtPhone.setError(null);
		txtAddress.setError(null);
		txtEmail.setError(null);
		txtPassword.setError(null);
	}
	
	//create the nameValuePairs needed for POST request
	public List<NameValuePair> generateNameValuePairs(){
    	List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
		nameValuePair.add(new BasicNameValuePair("name", txtName.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("surname", txtSurname.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("phone", txtPhone.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("address", txtAddress.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("email", txtEmail.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("password", txtPassword.getText().toString()));
		return nameValuePair;
	}
	
	 private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
        	
        	List<NameValuePair> nameValuePair = generateNameValuePairs();
            return Service.POST(urls[0], nameValuePair);
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	 Log.d("Response is: ",result);
        	 if(result.equalsIgnoreCase("NO CONNECTION"))
	            	Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_SHORT).show();
	            else
	            	//check whether the registration was successful 
        	if(!result.equals("OK")){
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Register.this);
        		alertDialogBuilder.setTitle("Your attention");
        		alertDialogBuilder.setMessage("Another user with the same email is already registered.\nPress 'Login' to login or 'Retry' to register with different email");
        		alertDialogBuilder.setCancelable(false);
        		alertDialogBuilder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Register.this, UserAuthentication.class);
						intent.putExtra(Service.USER_EMAIL, txtEmail.getText().toString());
						intent.putExtra(Service.USER_PASSWORD, txtPassword.getText().toString());
						startActivity(intent);
						Register.this.finish();
					}
				});
        		alertDialogBuilder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						txtEmail.setText("");
						txtEmailConfirm.setText("");
						txtEmail.setSelected(true);
						return;
					}
				});
        		
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
        	}else{
	            Toast.makeText(getBaseContext(), "Successfull!", Toast.LENGTH_LONG).show();
	            Log.d("Response is: ",result);
	            
	            //save user details
	            Service.userEmail = txtEmail.getText().toString();
            	Service.userName = txtName.getText().toString();
            	Service.userSurname = txtSurname.getText().toString();
            	Service.userPhone = txtPhone.getText().toString();
            	Service.userAddress = txtAddress.getText().toString();
            	Service.userBalance = 0.0;
            	Intent intent = new Intent(Register.this, OrderMenu.class);
            	startActivity(intent);
        	}
            
       }
    }//end HttpAsyncTask
	
}
