package com.zdeno.virtualdinerandroid;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class UserAuthentication extends ActionBarActivity {
	
	private EditText txtPassword;
	private EditText txtEmail;
	private static int incorrectLoginCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_authentication);
		Intent intent = getIntent();
		String email = intent.getStringExtra(Service.USER_EMAIL);
		String password = intent.getStringExtra(Service.USER_PASSWORD);
		
		//if the application has came here form registration when the same email was already in use, 
		//populate the email and password with the same details and let the user login
		if(email != null && password != null){
			EditText txtEmail = (EditText)findViewById(R.id.txtAuthenticationEmail);
			txtEmail.setText(email);
			EditText txtPassword = (EditText)findViewById(R.id.txtAuthenticationPassword);
			txtPassword.setText(password);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//logs the user in the application and redirects to OrderMenu
		public void login(View view){
			txtEmail = (EditText)findViewById(R.id.txtAuthenticationEmail);
			txtPassword = (EditText)findViewById(R.id.txtAuthenticationPassword);
			if(fieldsAreValid()){
				String emailString = txtEmail.getText().toString();
				String passwordString = txtPassword.getText().toString();
				String restUrl = null;
				Log.d("Email TExt", emailString);
				Log.d("Pass TExt", passwordString);
				if(isOnline()){
					try {
						//check if the server is up and running
						restUrl="getUserByEmailAndPassword?email="+
								URLEncoder.encode(emailString,"UTF-8")+"&password="+
								URLEncoder.encode(passwordString,"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					new HttpAsyncTask().execute(restUrl);
				}else{
					Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
				}
			}
		}
		
		
		//shows the fields necessary to sign up
		public void signUp(View view){
			Intent intent = new Intent(this, Register.class);
			startActivity(intent);
		}
		
		public boolean isOnline() {
		    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnected()) {
		        return true;
		    }
		    return false;
		}
	    
		//get user by email and password
	    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
	        @Override
	        protected String doInBackground(String... urls) {
	            return Service.GET(urls[0]);
	        }
	        
	        @Override
	        protected void onPostExecute(String result) {
	            Log.d("Response is: ",result);
	            if(result.equalsIgnoreCase("NO CONNECTION"))
	            	Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_SHORT).show();
	            else
		            try {
		            	//Expected result has this form: [["email","name","surname","phone","password"(mysql encoded),"address","balance"]]
		            	//OR User not found
		            	JSONArray jsonarray = new JSONArray(result);
		            	Service.userEmail = jsonarray.getJSONArray(0).getString(0);
		            	Service.userName = jsonarray.getJSONArray(0).getString(1);
		            	Service.userSurname = jsonarray.getJSONArray(0).getString(2);
		            	Service.userPhone = jsonarray.getJSONArray(0).getString(3);
		            	Service.userAddress = jsonarray.getJSONArray(0).getString(5);
		            	Service.userBalance = jsonarray.getJSONArray(0).getDouble(6);
		            	Toast.makeText(getBaseContext(), "Logged in", Toast.LENGTH_LONG).show();
		            	Intent intent = new Intent(UserAuthentication.this, OrderMenu.class);
		            	startActivity(intent);
		            	UserAuthentication.this.finish();
					} catch (JSONException e) {
						e.printStackTrace();
						removeErrors();
						
						//verify if the user entered a wrong password or a wrong username. Display error messages accordingly
						//if wrong password has been entered, retry 3 times and then offer password reset
						String restUrl = null;
			    		try {
			    			restUrl="getUserByEmail?email="+
			    					URLEncoder.encode(txtEmail.getText().toString(),"UTF-8");
			    		} catch (UnsupportedEncodingException ex) {
			    			ex.printStackTrace();
			    		}
			    		
			    		new HttpAsyncGetUser().execute(restUrl);
			    		
					}//end catch
	       }
	    }
	    
	    //get User by email
	    private class HttpAsyncGetUser extends AsyncTask<String, Void, String> {
	        @Override
	        protected String doInBackground(String... urls) {
	            return Service.GET(urls[0]);
	        }
	        
	        @Override
	        protected void onPostExecute(String result) {
	            Log.d("Response is: ",result);
	            if(result.equalsIgnoreCase("NO CONNECTION"))
	            	Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_SHORT).show();
	            else
	            if(result.contains("not found")){
		            txtEmail.setError("Incorrect Email");
		            txtEmail.setSelected(true);
		            return;
	            }else{
					incorrectLoginCount++;
					txtPassword.setError("Incorrect Password");
					txtPassword.setSelected(true);
					if(incorrectLoginCount > 2){
						//offer the user to reset their password
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UserAuthentication.this);
		        		alertDialogBuilder.setTitle("Can't remember?");
		        		alertDialogBuilder.setMessage("This is your 3rd time you entered an incorrect password\nWould you like to reset your password?");
		        		alertDialogBuilder.setCancelable(false);
		        		alertDialogBuilder.setPositiveButton("Yes, reset my password", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//reset password and send email
								String restUrl ="email/sendEmail/resetPassword";
					    		new HttpAsyncResetPassword().execute(restUrl);
							}
						});
		        		alertDialogBuilder.setNegativeButton("No, I'll try again", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								incorrectLoginCount = 0;
								txtPassword.setText("");
								txtPassword.setSelected(true);
								return;
							}
						});
		        		
						AlertDialog alertDialog = alertDialogBuilder.create();
						alertDialog.show();
						return;
					}
	            }
	       }
	    }//end HttpAsyncGetUser
	    
	    //reset password and send email
	    private class HttpAsyncResetPassword extends AsyncTask<String, Void, String> {
	    	LinearLayout linlaHeaderProgress = (LinearLayout) findViewById(R.id.llProgressBar);

	    	@Override
	    	protected void onPreExecute() {    
	    		incorrectLoginCount = 0;
	    	    // show spinner
	    	    linlaHeaderProgress.setVisibility(View.VISIBLE);
	    	    removeErrors();
	    	}
	    		
	        @Override
	        protected String doInBackground(String... urls) {
	        	List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
	    		nameValuePair.add(new BasicNameValuePair("email", txtEmail.getText().toString()));
	            return Service.POST(urls[0], nameValuePair);
	        }
	        
	        // onPostExecute displays the results of the AsyncTask.
	        @Override
	        protected void onPostExecute(String result) {
	        	// hide spinner
	    	    linlaHeaderProgress.setVisibility(View.GONE);
	            Log.d("Response is: ",result);
	            if(result.equalsIgnoreCase("NO CONNECTION"))
	            	Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_SHORT).show();
	            else
	            if(result.equals("OK")){
	            	//display message to user that their pass has been rest
	            	String title = "Password reset";
	            	String message = "Your password has been reset.\nAn email has been sent to your email address.\nUse that password to login but do not forget to change it afterwards.";
	            	AlertDialog alertDialog = buildAllert(title, message).create();
					alertDialog.show();
					return;
	            }else{
	            	//display message to the user that the pass has not bee reset due to unforeseen circumstances
	            	String title = "Unforeseen error";
	            	String message = "The earth has been swallowed or something simillar and we were not able to reset your password.";
	            	AlertDialog alertDialog = buildAllert(title, message).create();
					alertDialog.show();
					return;
	            }
	       }
	    }//end HttpAsyncGetUser
	    
	    //validate fields
	    public boolean fieldsAreValid(){
	    	removeErrors();
	    	if(txtEmail.getText().toString().equals("")){
	    		txtEmail.setError("Email cannot be null");
	    		return false;
	    	}else if(!txtEmail.getText().toString().matches(Service.EMAIL_PATTERN)){
	    		txtEmail.setError("Invalid Email");
	    		return false;
	    	}else if(txtPassword.getText().toString().length() <6){
	    		txtPassword.setError("Password must be at least 6 char long");
	    		return false;
	    	}
			return true;
	    }
	    
	    //remove all errors
	    public void removeErrors(){
			txtEmail.setError(null);
			txtPassword.setError(null);
		}
	    
	    //build an AlertDialog
	    public Builder buildAllert(String title, String message){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UserAuthentication.this);
			alertDialogBuilder.setTitle(title);
			alertDialogBuilder.setMessage(message);
			alertDialogBuilder.setCancelable(false);
			alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			return alertDialogBuilder;
	    }
	
}
