package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;

import android.util.Log;

import com.zdeno.virtualdinerandroid.support.CompleteItem;

public class FavoritePager extends Pager {
	

	
	//arrange items so that first item is the one user selected
	@Override
	public void createListOfItemsToDisplayInPageView() {
		arrangedItems = new ArrayList<CompleteItem>();
		for(CompleteItem item:Service.favorites){
			if(item.getName().equals(Service.selectedFavoriteItem)){
				arrangedItems.add(item);
				itemUserSelected = item;
				break;
			}
		}
		for(CompleteItem item:Service.favorites){
			if(!item.getName().equals(Service.selectedFavoriteItem)){
				arrangedItems.add(item);
			}
		}
	}//end createListOfItemsToDisplayInPageView
	
}//end FavoritePager
