package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.zdeno.virtualdinerandroid.support.CompleteItem;

public class ItemList extends ActionBarActivity implements OnGestureListener {
	ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();
	private ListView lv;
	GestureDetector detector;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_list);
		detector = new GestureDetector(this);
		lv = (ListView) findViewById(R.id.item_listview);

		displayItems();
	}

	private void displayItems() {
		Service.atomicItems = new ArrayList<CompleteItem>();
		// find products that have the selected category
		for (CompleteItem item : Service.items) {
			if (item.getCategory().equals(Service.seletedCategory)) {
				//only add unique items
				boolean found = false;
				for(CompleteItem c:Service.atomicItems){
					if(item.getName().equalsIgnoreCase(c.getName())){
						found = true;
						break;
					}
				}
				if(found == false){
					Service.atomicItems.add(item);
				}
			}
		}

		for (CompleteItem item : Service.atomicItems) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", item.getName());
			map.put("description", item.getDescriptionShort());
			itemList.add(map);
		}

		updateListView();

	}

	//populate the list
	private void updateListView() {
		ListAdapter adapter = new SimpleAdapter(ItemList.this, itemList,
				R.layout.list_item_item,
				new String[] { "name", "description" }, new int[] {
						R.id.list_item_name, R.id.list_item_description });
		// updating listview
		lv.setAdapter(adapter);

		// ListView Item Click Listener
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item value
				Service.selectedItem = ((TextView) view.findViewById(R.id.list_item_name)).getText().toString();
				
				//redirect
				Intent intent = new Intent(ItemList.this, Pager.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return detector.onTouchEvent(event);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		if (e2.getX() - e1.getX() > SWIPE_MAX_OFF_PATH
				&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
			// back button functionality
			finish();
		}
		return false;

	}

}
