package com.zdeno.virtualdinerandroid;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdeno.virtualdinerandroid.support.CompleteItemWithAmount;
import com.zdeno.virtualdinerandroid.support.OrderToSubmit;

public class CurrentOrder extends ActionBarActivity {
	ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();
	private ListView lv;
	double total = 0;
	View touchedView;
	int mDownX, mDownPosition = 0;
	private View.OnTouchListener touchListener;
	private ListAdapter adapter;
	private TextView txtTotal;
	private DecimalFormat df = new DecimalFormat("#.00");
	private ArrayList<OrderToSubmit> listToSubmit;
	private ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_current_order);
		lv = (ListView) findViewById(R.id.current_order_item_listview);

		if (Service.currentOrder.size() == 0) {
			Toast.makeText(getApplicationContext(), "Your order is empty",
					Toast.LENGTH_SHORT).show();
			return;
		}

		// load current order items in the list
		displayItems();
	}

	// loop through list of ordered items and calculate total / put relevant
	// details in a hashmap that will represent the content of a listview
	private void displayItems() {
		int counter = 0;
		for (CompleteItemWithAmount item : Service.currentOrder) {
			double itemTotal = item.getPrice() * item.getAmount();
			total += itemTotal;

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("list_ordered_item_name", item.getName());
			map.put("list_ordered_item_size", item.getSize());
			map.put("list_ordered_item_amount",
					String.valueOf(item.getAmount()));
			map.put("list_ordered_item_total", df.format(itemTotal));
			map.put("position", String.valueOf(counter));
			counter++;
			itemList.add(map);
		}
		txtTotal = (TextView)findViewById(R.id.total_value);
		txtTotal.setText("€" + String.valueOf(df.format(total)));

		updateListView();

		//touchListener
		touchListener = new View.OnTouchListener() {
			
			private float downX;
			private int touchSlop = -1;
			private boolean itemPressed = false;
			private boolean swiping = false;
			View thisView;
			
			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
			public boolean onTouch(View v, MotionEvent event) {
				thisView = v;
				//get the touch slop of the current device (the amount of pixels needed to move finger on screen in order to be recorded as scroll action 
				if(touchSlop < 0)
					touchSlop = ViewConfiguration.get(v.getContext()).getScaledTouchSlop();
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {
					//check if the user pressed more than one item. If so then return false and do not handle it
					if(itemPressed)
						return false;
					itemPressed = true;
					downX = event.getX();
					break; 
				}
				case MotionEvent.ACTION_CANCEL:{
					v.setAlpha(1); //sets the opacity of the view to 100%
					v.setTranslationX(0); //sets the horizontal location (left) of this view to 0 points in relation to its parent
					itemPressed = false;
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					float x = event.getX() + v.getTranslationX(); //get the touch position in relation to it's current location in a parent
					float deltaX = x - downX; //difference between current location and initial touch
					float deltaAbsX = Math.abs(deltaX); // get the absolute number, ignore (-) 
					
					//check whether we are swiping or not
					if(swiping == false){
						if(deltaAbsX > touchSlop){
							swiping = true;
							//tell the parent to stop intercepting other swipe events so that this view has full attention
							v.getParent().requestDisallowInterceptTouchEvent(true);
						}
					}
					if(swiping == true){
						v.setTranslationX(x-downX);
						v.setAlpha(1 - deltaAbsX / v.getWidth());
					}
					break;
				}
				case MotionEvent.ACTION_UP: {
					if(swiping == true){
						int position = 0;
						float x = event.getX() + v.getTranslationX();
						float deltaX = x - downX;
						float deltaAbsX = Math.abs(deltaX);
						if(deltaAbsX > v.getWidth() / 3){
							//v.setAlpha(0);
							LinearLayout ll = (LinearLayout)v;
							LinearLayout ll1 = (LinearLayout) ll.getChildAt(0);
							TextView txtName = (TextView)ll1.getChildAt(0);
							TextView txtSize = (TextView)ll1.getChildAt(1);
							
							for(int i=0; i< adapter.getCount(); i++){
								HashMap<String, String> map = (HashMap<String, String>) adapter.getItem(i);
								if(map.get("list_ordered_item_size").equalsIgnoreCase(txtSize.getText().toString()) &&
										map.get("list_ordered_item_name").equalsIgnoreCase(txtName.getText().toString())){
									position = i;
									Log.d("itemList","Removed item from position: "+i+". The item was: "+Service.currentOrder.get(position).getName());
									double itemTotal = Service.currentOrder.get(position).getAmount() * Service.currentOrder.get(position).getPrice();
									double total = Double.parseDouble(txtTotal.getText().toString().substring(1, txtTotal.getText().toString().length()));
									total-= itemTotal;
									txtTotal.setText(String.valueOf(df.format(total)));
									itemList.remove(position);
									Service.currentOrder.remove(position);
									MyListAdapter ad = (MyListAdapter)lv.getAdapter();
									ad.notifyDataSetChanged();
									v.setTranslationX(0);
									v.setAlpha(1);
									break;
								}
							}
						}else{
							v.setTranslationX(0);
							v.setAlpha(1);
						}
					}
					itemPressed = false;
					break;
				}
				default:{
					return false;
				}
				}//end switch
				return true;
			}//end onTouch
		};
	}
	

	// populate the list
	private void updateListView() {
		adapter = new MyListAdapter(CurrentOrder.this,R.layout.list_ordered_item, itemList, touchListener);
		// updating listview
		lv.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//send the order to server
	public void processOrder(View view){
		if(Service.currentOrder.size()>0){
			listToSubmit = new ArrayList<OrderToSubmit>();
			for(CompleteItemWithAmount item : Service.currentOrder){
				OrderToSubmit o = new OrderToSubmit();
				o.setName(item.getName());
				o.setId(item.getId());
				o.setPrice((float)item.getPrice());
				o.setAmount(item.getAmount());
				o.setSize(o.getSize());
				o.setTotal(Float.parseFloat(df.format(item.getAmount()*item.getPrice())));
				listToSubmit.add(o);
			}
			showAlert();
		}else{
			Toast.makeText(getApplicationContext(), "Nothing to order", Toast.LENGTH_SHORT).show();
		}
	}

	private void showAlert() {
		LayoutInflater li = getLayoutInflater();
		View v = li.inflate(R.layout.alert_with_comment, null);
		final EditText et = (EditText) v.findViewById(R.id.comment);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CurrentOrder.this);
		alertDialogBuilder.setTitle("Additional information required.");
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.setView(v);
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		Log.d("DIALOG",listToSubmit.toString());
		
		Button submit = (Button)v.findViewById(R.id.btnSubmitOrderFromDialog);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//check if user has entered table ID
				EditText tableId = (EditText) ((LinearLayout) v.getParent()).findViewById(R.id.tableId);
				if(tableId.getText().toString().equals("")){
					tableId.setError("You must provide table ID");
					return;
				}
				
				//set tableID
				listToSubmit.get(0).setTableId(tableId.getText().toString());
				
				if(Service.userName != ""){
					//add the comment as before-last item in the list 
					OrderToSubmit o = new OrderToSubmit();
					o.setName(et.getText().toString());
					o.setId(0);
					o.setPrice(0);
					o.setAmount(0);
					o.setSize("");
					o.setTotal(0);
					listToSubmit.add(o);
					//add the userName as the last item in the list
					o = new OrderToSubmit();
					o.setName(Service.userEmail);
					o.setId(0);
					o.setPrice(0);
					o.setAmount(0);
					o.setSize("");
					o.setTotal(0);
					listToSubmit.add(o);
					Log.d("DIALOG USER",listToSubmit.toString());
				}else{
					//add the comment as the last item in the list
					OrderToSubmit o = new OrderToSubmit();
					o.setName(et.getText().toString());
					o.setId(0);
					o.setPrice(0);
					o.setAmount(0);
					o.setSize("");
					o.setTotal(0);
					listToSubmit.add(o);
					Log.d("DIALOG NO USER",listToSubmit.toString());
				}
				
				//send the order to server finally
				AsyncTask<String, Void, String> task = new AsyncTask<String,Void,String>(){

					@Override
					protected void onPreExecute() {
						super.onPreExecute();
						pDialog = new ProgressDialog(CurrentOrder.this);
						pDialog.setMessage("Sending order ...");
						pDialog.setIndeterminate(false);
						pDialog.setCancelable(false);
						pDialog.show();
					}

					@Override
					//http://stackoverflow.com/q/4841952
					protected String doInBackground(String... params) {
						JSONArray array = new JSONArray();
						for(OrderToSubmit o:listToSubmit){
							array.put(o.getJsonObjectEqvivalent());
						}
			            return Service.POST(params[0], array);
					}
					
					@Override
					protected void onPostExecute(String result) {
						pDialog.dismiss();
						if(result.equalsIgnoreCase("OK")){
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CurrentOrder.this);
							alertDialogBuilder.setCancelable(false);
							alertDialogBuilder.setTitle("You'll be pleased to know");
							alertDialogBuilder.setMessage("Your order has been sent successfully. Our staff will serve you momentarity.\n\nEnjoy your meal");
							alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent intent = new Intent(CurrentOrder.this,Options.class);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);
									return;
								}
							});
							AlertDialog alertDialog = alertDialogBuilder.create();
							alertDialog.show();
							//clear the order
							Service.currentOrder = new ArrayList<CompleteItemWithAmount>();
						}else{
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CurrentOrder.this);
							alertDialogBuilder.setCancelable(false);
							alertDialogBuilder.setTitle("Hmm, that's strange");
							alertDialogBuilder.setMessage("We could not process your order due to unknown error.\nCould you please try again?\nIf this error persist ask one of our staff members for assistance.");
							alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									return;
								}
							});
							AlertDialog alertDialog = alertDialogBuilder.create();
							alertDialog.show();
						}
					}
				};
				
				if(Service.userEmail == "")
					task.execute("sendOrderAsUnknownCustomer");
				else
					task.execute("sendOrder");
			}
		});
	}

	public class MyListAdapter extends ArrayAdapter<HashMap<String, String>> {

		public MyListAdapter(Context context, int resource,
				List<HashMap<String, String>> objects, View.OnTouchListener touchListener) {
			super(context, resource, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				Log.d("VIEW", "v is null");
				LayoutInflater vi;
				vi = LayoutInflater.from(getContext());
				v = vi.inflate(R.layout.list_ordered_item, null);
				v.setOnTouchListener(touchListener);
			}

			HashMap<String, String> map = getItem(position);

			if (map != null) {

				TextView tv = (TextView) v
						.findViewById(R.id.list_ordered_item_name);
				TextView tv1 = (TextView) v
						.findViewById(R.id.list_ordered_item_amount);
				TextView tv2 = (TextView) v
						.findViewById(R.id.list_ordered_item_total);
				TextView tv3 = (TextView) v
						.findViewById(R.id.list_ordered_item_size);

				if (tv != null) {
					tv.setText(map.get("list_ordered_item_name"));
				}
				if (tv1 != null) {
					tv1.setText(map.get("list_ordered_item_amount"));
				}
				if (tv2 != null) {
					tv2.setText("€" + map.get("list_ordered_item_total"));
				}
				if(tv3 != null) {
					tv3.setText(map.get("list_ordered_item_size"));
				}
			}

			return v;
		}

	}

}