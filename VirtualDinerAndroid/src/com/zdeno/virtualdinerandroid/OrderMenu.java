package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.zdeno.virtualdinerandroid.support.CompleteItem;

public class OrderMenu extends ActionBarActivity {
	private ProgressDialog pDialog;
	private static final String REST_URL = "getAllItems";
	ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_list);
		if(isOnline()){
		lv = (ListView) findViewById(R.id.category_listview); // ListView in activity
		// load items
		if (Service.items.isEmpty())
			new LoadItems().execute(REST_URL);
			new DisplayCategories().execute("");
		}else{
			Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}

	// load all food items from the server and save them in an array
	private class LoadItems extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(OrderMenu.this);
			pDialog.setMessage("Loading Menu ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Inbox JSON
		 * */
		protected String doInBackground(String... args) {
			String result = Service.GET(args[0]);
			Log.d("RESULT", result);

			try {
				JSONArray jsonArray = new JSONArray(result);
				Service.items = new ArrayList<CompleteItem>();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					CompleteItem c = new CompleteItem();
					c.setId(object.getInt("id"));
					c.setPrice(object.getDouble("price"));
					c.setSize(object.getString("size"));
					c.setName(object.getString("name"));
					c.setAllergy(object.getString("allergy"));
					c.setCategory(object.getString("category").substring(2,object.getString("category").length()));
					c.setDescriptionLong(object.getString("descriptionLong"));
					c.setDescriptionShort(object.getString("descriptionShort"));
					c.setGrouping(object.getString("grouping"));
					c.setImageName(object.getString("imageName"));
					c.setIngredients(object.getString("ingredients"));
					c.setCarbs(object.getDouble("carbs"));
					c.setFat(object.getDouble("fat"));
					c.setFibre(object.getDouble("fibre"));
					c.setKcalories(object.getDouble("kcalories"));
					c.setProtein(object.getDouble("protein"));
					c.setSalt(object.getDouble("salt"));
					c.setSaturates(object.getDouble("saturates"));
					c.setSugar(object.getDouble("sugar"));
					Service.items.add(c);
				}
			} catch (JSONException e) {
				Toast.makeText(getApplicationContext(),
						"Couldn't get food items", Toast.LENGTH_SHORT).show();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
		}

	}

	private class DisplayCategories extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// find categories from products
			if (Service.categories.isEmpty()) {
				for (CompleteItem item : Service.items) {
					String cat = item.getCategory();
					if (!Service.categories.contains(cat)) {
						Service.categories.add(cat);
					}
				}
			}

			for (String category : Service.categories) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("name", category);
				itemList.add(map);
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			// updating UI from Background Thread
			ListAdapter adapter = new SimpleAdapter(OrderMenu.this,
					itemList, R.layout.list_item_category,
					new String[] { "name" },
					new int[] { R.id.category_item_name });
			// updating listview
			lv.setAdapter(adapter);

			// ListView Item Click Listener
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					// ListView Clicked item value
					String categoryName = ((TextView) view.findViewById(R.id.category_item_name)).getText().toString();
					Service.seletedCategory = categoryName;
					Intent intent = new Intent(OrderMenu.this, ItemList.class);
					startActivity(intent);
				}
			});
		}
	}

}
