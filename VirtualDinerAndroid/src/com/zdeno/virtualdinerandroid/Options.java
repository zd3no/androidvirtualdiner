package com.zdeno.virtualdinerandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Options extends Activity {
	Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Checking if User is logged in","Checking...............................");
		//if user is logged in then display activity for customer only
		if(Service.userEmail.length() == 0){
			setContentView(R.layout.activity_options_customer);
		}else{
			setContentView(R.layout.activity_options_user);
		}
	}
	
	public void goToOrderMenu(View view){
		intent = new Intent(this, OrderMenu.class);
		startActivity(intent);
	}
	
	public void goToContact(View view){
		intent = new Intent(this, Contact.class);
		startActivity(intent);
	}
	
	public void goToLogout(View view){
		Toast.makeText(getApplicationContext(), "You have been logged Out successfully", Toast.LENGTH_SHORT).show();;
		Service.clearSession();
		intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	
	public void goToFavorites(View view){
		intent = new Intent(this, Favorites.class);
		startActivity(intent);
	}
	
	public void goToProfile(View view){
		intent = new Intent(this, Profile.class);
		startActivity(intent);
	}
	
	public void goToCurrentOrder(View view){
		intent = new Intent(this, CurrentOrder.class);
		startActivity(intent);
	}
	
	public void goToDisabledButton(View view){
		Toast.makeText(getApplicationContext(), "Available only to registered users", Toast.LENGTH_SHORT).show();
	}
}