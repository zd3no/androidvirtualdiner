package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zdeno.virtualdinerandroid.support.CompleteItem;
import com.zdeno.virtualdinerandroid.support.CompleteItemWithAmount;

//http://developer.android.com/reference/android/support/v4/app/FragmentStatePagerAdapter.html
public class Pager extends ActionBarActivity {

	static CompleteItem itemUserSelected;
	static ArrayList<CompleteItem> arrangedItems;
	private static Menu thisMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pager);
		
		createListOfItemsToDisplayInPageView();
		
		//create ViewPager and it's adapter
		PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
		ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mPagerAdapter);
		
		Service.setPagerContext(getApplicationContext());
	}

	//remove duplicates and arrange items so that first item is the one user selected
	public void createListOfItemsToDisplayInPageView() {
		arrangedItems = new ArrayList<CompleteItem>();
		for(CompleteItem item:Service.atomicItems){
			if(item.getName().equals(Service.selectedItem) && item.getCategory().equals(Service.seletedCategory)){
				arrangedItems.add(item);
				itemUserSelected = item;
				break;
			}
		}
		for(CompleteItem item:Service.atomicItems){
			if(item.getCategory().equals(Service.seletedCategory))
			if(!item.getName().equals(Service.selectedItem) && !arrangedItems.contains(item)){
				arrangedItems.add(item);
			}
		}
	}

	//Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		thisMenu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	//Menu SelectedItem
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * FragmentStatePagerAdapter that displays fragments depending on the size of "arrangedItems" array
	 */
	public static class PagerAdapter extends FragmentStatePagerAdapter {

		public PagerAdapter(FragmentManager fm){
			super(fm);
		}
		

		@Override
		public Fragment getItem(int position) {
			//PlaceholderFragment is static and should not be instantiated from here.
			return PlaceholderFragment.newInstance(position);
		}

		@Override
		public int getCount() {
			return arrangedItems.size();
		}

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		CompleteItem item;
		TextView name, descriptionLong, ingredients, allergy, kcalories,protein, carbs, fat, saturates,
		fibre, sugar, salt, priceLarge, priceSmall, priceMedium, sizeLarge,sizeSmall,sizeMedium;
		LinearLayout llLarge,llSmall,llMedium;
		Button btnSmall,btnLarge,btnMedium;
		ImageView makeFavorite, image;
		View rootView;
				
		//create a new instance using empty constructor (must use empty constructor)
		public static PlaceholderFragment newInstance(int index){
			PlaceholderFragment fragment = new PlaceholderFragment();
			
			// Supply index input as an argument. This index indicates which item from the array of "arrangedItems" 
			// will be displayed in this fragment's view
	        Bundle args = new Bundle();
	        args.putInt("index", index);
	        fragment.setArguments(args);
			return fragment;
		}
		
		//extract the index from getArguments() which is used to create an instance of the item we need to display in the view
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        int number = 0;
	        if(getArguments() != null){
	        	number = getArguments().getInt("index");
	        }else{
	        	number = 1;
	        }
	        item = arrangedItems.get(number);
	    }
		
		//inflate the view with the content of R.id.fragment_pager (ScrollView)
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			rootView = inflater.inflate(R.layout.fragment_pager, container, false);
			
			getReferencesForElementsFromRootView();
			setNewValuesToElements();
			return rootView;
		}

		/**
		 * Instantiate elements of the fragment_pager view. They can later be used to change
		 * their content on the view
		 */
		private void getReferencesForElementsFromRootView() {
			name = (TextView) rootView.findViewById(R.id.fragment_item_name);
			descriptionLong = (TextView) rootView.findViewById(R.id.fragment_item_description);
			ingredients = (TextView) rootView.findViewById(R.id.fragment_item_ingredients);
			allergy = (TextView) rootView.findViewById(R.id.fragment_item_allergy);
			kcalories = (TextView) rootView.findViewById(R.id.fragment_item_kcalories);
			protein = (TextView) rootView.findViewById(R.id.fragment_item_protein);
			carbs = (TextView) rootView.findViewById(R.id.fragment_item_carbs);
			fat = (TextView) rootView.findViewById(R.id.fragment_item_fat);
			saturates = (TextView) rootView.findViewById(R.id.fragment_item_saturates);
			fibre = (TextView) rootView.findViewById(R.id.fragment_item_fibre);
			sugar = (TextView) rootView.findViewById(R.id.fragment_item_sugar);
			salt = (TextView) rootView.findViewById(R.id.fragment_item_salt);
			priceLarge = (TextView) rootView.findViewById(R.id.profile_item_price_large);
			priceSmall = (TextView) rootView.findViewById(R.id.profile_item_price_small);
			priceMedium = (TextView) rootView.findViewById(R.id.profile_item_price_medium);
			sizeLarge = (TextView) rootView.findViewById(R.id.profile_item_size_large);
			sizeSmall = (TextView) rootView.findViewById(R.id.profile_item_size_small);
			sizeMedium = (TextView) rootView.findViewById(R.id.profile_item_size_medium);
			llLarge = (LinearLayout) rootView.findViewById(R.id.profile_linear_layout_large);
			llSmall = (LinearLayout) rootView.findViewById(R.id.profile_linear_layout_small);
			llMedium = (LinearLayout) rootView.findViewById(R.id.profile_linear_layout_medium);
			btnLarge = (Button) rootView.findViewById(R.id.profile_item_button_large);
			btnSmall = (Button) rootView.findViewById(R.id.profile_item_button_small);
			btnMedium = (Button) rootView.findViewById(R.id.profile_item_button_medium);
			makeFavorite = (ImageView) rootView.findViewById(R.id.make_favorite);
			image = (ImageView) rootView.findViewById(R.id.fragment_item_image);
		}
		
		/**
		 * Update view's elements values according to the item we want to display
		 */
		private void setNewValuesToElements() {
			name.setText(item.getName());
			descriptionLong.setText(item.getDescriptionLong());
			ingredients.setText(item.getIngredients());
			allergy.setText(item.getAllergy());
			kcalories.setText(String.valueOf(item.getKcalories()));
			protein.setText(String.valueOf(item.getProtein()));
			carbs.setText(String.valueOf(item.getCarbs()));
			fat.setText(String.valueOf(item.getFat()));
			saturates.setText(String.valueOf(item.getSaturates()));
			sugar.setText(String.valueOf(item.getSugar()));
			salt.setText(String.valueOf(item.getSalt()));
			fibre.setText(String.valueOf(item.getFibre()));
			
			//if user is not logged in then do not display favorites star
			if(Service.userEmail.length() == 0){
				makeFavorite.setVisibility(View.GONE);
			}
			
			//make the bitmap to display image
			String imageURL = Service.DOMAIN_URL_IMAGES+item.getImageName();
			new GetImageFromServer().execute(imageURL);
			
			//find which sizes to display
			for(CompleteItem c:Service.items){
				if(item.getName().equals(c.getName())){
					String size = c.getSize();
					if(size.equalsIgnoreCase("individual")){
						llLarge.setVisibility(View.VISIBLE);
						sizeLarge.setText("Individual");
						priceLarge.setText("€" + String.valueOf(c.getPrice()));
						btnLarge.setOnClickListener(new ButtonClickListener(c));
						btnLarge.setOnLongClickListener(new ButtonLongClickListener(c));
					}else if(size.equalsIgnoreCase("large")){
						llLarge.setVisibility(View.VISIBLE);
						sizeLarge.setText("Large");
						priceLarge.setText("€" + String.valueOf(c.getPrice()));
						btnLarge.setOnClickListener(new ButtonClickListener(c));
						btnLarge.setOnLongClickListener(new ButtonLongClickListener(c));
					}else if(size.equalsIgnoreCase("medium")){
						llMedium.setVisibility(View.VISIBLE);
						sizeMedium.setText("Medium");
						priceMedium.setText("€" + String.valueOf(c.getPrice()));
						btnMedium.setOnClickListener(new ButtonClickListener(c));
						btnMedium.setOnLongClickListener(new ButtonLongClickListener(c));
					}else if(size.equalsIgnoreCase("small")){
						llSmall.setVisibility(View.VISIBLE);
						sizeSmall.setText("Small");
						priceSmall.setText("€" + String.valueOf(c.getPrice()));
						btnSmall.setOnClickListener(new ButtonClickListener(c));
						btnSmall.setOnLongClickListener(new ButtonLongClickListener(c));
					}
				}
			}
			
			makeFavorite.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					new UpdateFavorite().execute("setFavorite");
					return true;
				}
			});
		}
		
		/**
		 *onClickListener for "Add" buttons on this view. It asks the user for quantity and adds selected item to order
		 */
		public class ButtonClickListener implements OnClickListener{
			private CompleteItem itemClicked;
			
			public ButtonClickListener(CompleteItem itemClicked){
				this.itemClicked = itemClicked;
			}
			
			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
			@Override
			public void onClick(View v) {
				//vefiry if another item has already been set
				CompleteItemWithAmount completeItemWithAmount = new CompleteItemWithAmount(itemClicked, 1);
				boolean foundItemAlreadyInTheItemList = false;
				for(CompleteItemWithAmount c:Service.currentOrder){
					if(c.getName().equalsIgnoreCase(completeItemWithAmount.getName())&&
							c.getSize().equalsIgnoreCase(completeItemWithAmount.getSize()))
					{
						foundItemAlreadyInTheItemList = true;
						c.setAmount(c.getAmount()+1);
						break;
					}
				}
				if(!foundItemAlreadyInTheItemList){
					Service.currentOrder.add(completeItemWithAmount);
				}
                Toast.makeText(getActivity(), "Added", Toast.LENGTH_SHORT).show();
                Thread thread = new Thread(new Runnable(){

					@Override
					public void run() {
						final MenuItem settings = thisMenu.findItem(R.id.action_settings);
			            
			            //http://techiedreams.com/android-rss-reader-part-3-action-bar-with-animated-item/
			            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			            ImageView iv = (ImageView) inflater.inflate(R.layout.action_bar_custom_image,null);
			            			
			            Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.drawable.highlight_settings);
			            rotation.setRepeatCount(1);
			            rotation.setAnimationListener(new AnimationListener() {
							
							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub
								
							}
							
							@TargetApi(Build.VERSION_CODES.HONEYCOMB)
							@Override
							public void onAnimationEnd(Animation animation) {
								settings.getActionView().clearAnimation();
								settings.setActionView(null);
							}
						});
			            iv.startAnimation(rotation);
			            settings.setActionView(iv);
					}
                	
                });
                thread.run();
			}
			
		}//end ButtonClickListener
		
		/**
		 *onLongClickListener that will ask the user to put amount of items to order instead of adding them one by one 
		 */
		public class ButtonLongClickListener implements OnLongClickListener{
			private CompleteItem itemClicked;
			private final String[] choice = {"1","2","3","4","5"};
			private int amount = 0;
			
			public ButtonLongClickListener(CompleteItem itemClicked){
				this.itemClicked = itemClicked;
			}
			
			@Override
			public boolean onLongClick(View v) {
				//display choices in a list.
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Amount to order");
			        builder.setItems(choice, new DialogInterface.OnClickListener() {
			        	
			            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
						public void onClick(DialogInterface dialog, int item) {
			                amount = Integer.parseInt(choice[item]);
			                CompleteItemWithAmount completeItemWithAmount = new CompleteItemWithAmount(itemClicked, amount);
			                boolean foundItemAlreadyInTheItemList = false;
			                for(CompleteItemWithAmount c:Service.currentOrder){
								if(c.getName().equalsIgnoreCase(completeItemWithAmount.getName())&&
										c.getSize().equalsIgnoreCase(completeItemWithAmount.getSize()))
								{
									foundItemAlreadyInTheItemList = true;
									c.setAmount(c.getAmount()+amount);
									break;
								}
							}
			                if(!foundItemAlreadyInTheItemList){
								Service.currentOrder.add(completeItemWithAmount);
							}
			                Toast.makeText(getActivity(), "Added", Toast.LENGTH_SHORT).show();
			                final MenuItem settings = thisMenu.findItem(R.id.action_settings);
			                Thread thread = new Thread(new Runnable(){

								@Override
								public void run() {
						            
						            //http://techiedreams.com/android-rss-reader-part-3-action-bar-with-animated-item/
						            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						            ImageView iv = (ImageView) inflater.inflate(R.layout.action_bar_custom_image,null);
						            			
						            Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.drawable.highlight_settings);
						            rotation.setRepeatCount(1);
						            rotation.setAnimationListener(new AnimationListener() {
										
										@Override
										public void onAnimationStart(Animation animation) {
											// TODO Auto-generated method stub
											
										}
										
										@Override
										public void onAnimationRepeat(Animation animation) {
											// TODO Auto-generated method stub
											
										}
										
										@TargetApi(Build.VERSION_CODES.HONEYCOMB)
										@Override
										public void onAnimationEnd(Animation animation) {
											settings.getActionView().clearAnimation();
											settings.setActionView(null);
										}
									});
						            iv.startAnimation(rotation);
						            settings.setActionView(iv);
								}
			                	
			                });
			                thread.run();
			            }

			        });
			        AlertDialog alert = builder.create();
			        alert.show();
			        return false;
			}
			
		}
		
		private class UpdateFavorite extends AsyncTask<String, Void, String> {
	        @Override
	        protected String doInBackground(String... urls) {
	        	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        	nameValuePairs.add(new BasicNameValuePair("email",Service.userEmail));
	        	nameValuePairs.add(new BasicNameValuePair("itemID", String.valueOf(item.getId())));
	        	
	            return Service.POST(urls[0], nameValuePairs);
	        }
	        // onPostExecute displays the results of the AsyncTask.
	        @Override
	        protected void onPostExecute(String result) {
	            Log.d("Response is: ",result);
	            if(result.equals("OK")){
	            	Toast.makeText(getActivity(), "Added to favorites", Toast.LENGTH_SHORT).show();
	            }else if(result.equals("DUPLICATE")){
	            	Toast.makeText(Service.getPagerContext().getApplicationContext(), "Already in favorites", Toast.LENGTH_SHORT).show();
	           }
	       }
	    }//end HttpAsyncGetUser
		
		//async task to load image from internet
		private class GetImageFromServer extends AsyncTask<String, Void, Drawable>{

			@Override
			protected Drawable doInBackground(String... params) {
				return Service.loadImageFromWebOperations(params[0]);
			}
			
			@Override
	        protected void onPostExecute(Drawable result) {
				Drawable draw = result;
				image.setImageDrawable(draw);
			}
		}
		
	}//end PlaceholderFragment
	
	
}//end Pager
