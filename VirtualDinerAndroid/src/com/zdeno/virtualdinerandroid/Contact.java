package com.zdeno.virtualdinerandroid;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Contact extends ActionBarActivity {
	EditText txtName;
	EditText txtEmail;
	EditText txtPhone;
	EditText txtMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);
		
		txtName = (EditText)findViewById(R.id.txtContactName);
		txtEmail = (EditText)findViewById(R.id.txtContactEmail);
		txtPhone = (EditText)findViewById(R.id.txtContactPhone);
		txtMessage = (EditText)findViewById(R.id.txtContactMessage);
		
		if(Service.userEmail != ""){
			txtName.setText(Service.userName);
			txtEmail.setText(Service.userEmail);
			txtPhone.setText(Service.userPhone);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, Options.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void sendMail(View view){
		if(fieldsAreValid()){
			if(isOnline()){
				//create and call POST request
				String restMethodURL = "email/sendEmail";
				new HttpAsyncTask().execute(restMethodURL);
			}else{
				Toast.makeText(getApplicationContext(), "No internet access", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}
	
	//send email
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		LinearLayout linlaHeaderProgress = (LinearLayout) findViewById(R.id.llProgressBarContact);
		
		@Override
		protected void onPreExecute(){
			linlaHeaderProgress.setVisibility(View.VISIBLE);
			removeErrors();
		}
		
        @Override
        protected String doInBackground(String... urls) {
        	
        	List<NameValuePair> nameValuePair = generateNameValuePairs();
            return Service.POST(urls[0], nameValuePair);
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	Log.d("Response is: ",result);
        	linlaHeaderProgress.setVisibility(View.GONE);
        	if(result.equals("OK")){
        		Toast.makeText(getApplicationContext(), "Email sent successfully", Toast.LENGTH_SHORT).show();
        		//clear text fields
        		txtMessage.setText("");
        	}else{
        		//display alert dialog
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Contact.this);
        		alertDialogBuilder.setTitle("Couldn't send message");
        		alertDialogBuilder.setMessage("We could not send the mail due to unforseen circumstances");
        		alertDialogBuilder.setCancelable(false);
        		alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
        		
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
        	}
        	//check whether the registration was successful 
        	
       }
    }//end HttpAsyncTask
	
	public boolean fieldsAreValid(){
		removeErrors();
		if(txtName.getText().toString().equals("")){
			txtName.setError("Name cannot be null");
			return false;
		}else if(!txtName.getText().toString().matches(Service.NAME_PATTERN)){
			txtName.setError("Invalid name");
			return false;
		}else if(txtPhone.getText().toString().equals("")){
			txtPhone.setError("Phone cannot be null");
			return false;
		}else if(!txtPhone.getText().toString().matches(Service.PHONE_PATTERN)){
			txtPhone.setError("Invalid phone number");
			return false;
		}else if(txtEmail.getText().toString().equals("")){
			txtEmail.setError("Email cannot be null");
			return false;
		}else if(!txtEmail.getText().toString().matches(Service.EMAIL_PATTERN)){
			txtEmail.setError("Invalid Email!");
			return false;
		}else if(txtMessage.getText().toString().equals("")){
			txtMessage.setError("Message cannot be null");
			return false;
		}else{
			return true;
		}
	}
	
	public void removeErrors(){
		txtName.setError(null);
		txtPhone.setError(null);
		txtEmail.setError(null);
		txtMessage.setError(null);
	}
	
	//create the nameValuePairs needed for POST request
	public List<NameValuePair> generateNameValuePairs(){
    	List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
    	nameValuePair.add(new BasicNameValuePair("FROM", txtEmail.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("NAME", txtName.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("PHONE", txtPhone.getText().toString()));
		nameValuePair.add(new BasicNameValuePair("MESSAGE", txtMessage.getText().toString()));
		return nameValuePair;
	}
	
}//end Contact
